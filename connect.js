var awsIot = require('aws-iot-device-sdk');
var device = awsIot.device(
{
  	port: 8883,
  	host: "A2TMY0JO4ITZ48.iot.us-west-2.amazonaws.com",
	keyPath: 'd974cd8665-private.pem.key',
  	certPath: 'd974cd8665-certificate.pem.crt',
    	caPath: 'root-CA.crt',
  	clientId: 'MicroGatewayClient1',
    	region: 'eu-west-1'
});
console.log("Device Connecting...");
device
  .on('connect', function() {
    console.log('Device Connected...');
    setInterval(function() {
		device.publish('scentair/things/microgateway1', JSON.stringify(
            { 
                "ts":new Date().getTime(),
                "type":"data",
                "values":[
                    {
                        "name":"voltage",
                        "value":Math.random()
                    },
                    {
                        "name":"current",
                        "value":Math.random()
                    },
                    {
                        "name":"fan_speed",
                        "value":Math.random()
                    },
                    {
                        "name":"fan_current",
                        "value":Math.random()
                    },
                    {
                        "name":"pump_speed",
                        "value":Math.random()
                    },
                    {
                        "name":"pump_current",
                        "value":Math.random()
                    },
                    {
                        "name":"liquid_fluid",
                        "value":Math.random()
                    }
                ]
            }));	
        console.log("Message Published...");
	},10000);		
});

