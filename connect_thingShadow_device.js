
var awsIot = require('aws-iot-device-sdk');
var thingShadows = awsIot.thingShadow(
{
  "host": "A2TMY0JO4ITZ48.iot.us-west-2.amazonaws.com",
	"port": 8883,
	"clientId": "MicroGateway1",
	"thingName": "MicroGateway1",
	"caCert": "root-CA.crt",
	"clientCert": "d974cd8665-certificate.pem.crt",
	"privateKey": "d974cd8665-private.pem.key"
});
console.log("Device Connecting...");



var clientTokenUpdate;

//
// Simulated device values
//
var turnONVal = 'ON';


thingShadows.on('connect', function() {
//
// After connecting to the AWS IoT platform, register interest in the
// Thing Shadow named 'RGBLedLamp'.
//
  thingShadows.register( 'MicroGateway1');
//
// 5 seconds after registering, update the Thing Shadow named
// 'RGBLedLamp' with the latest device state and save the clientToken
// so that we can correlate it with status or timeout events.
//
// Note that the delay is not required for subsequent updates; only
// the first update after a Thing Shadow registration using default
// parameters requires a delay.  See API documentation for the update
// method for more details.
//
  setTimeout( function() {
//
// Thing shadow state
//
     var rgbLedLampState = {"state":{"reported":{"turnON":turnONVal}}};

     clientTokenUpdate = thingShadows.update('MicroGateway1', rgbLedLampState  );
//
// The update method returns a clientToken; if non-null, this value will
// be sent in a 'status' event when the operation completes, allowing you
// to know whether or not the update was successful.  If the update method
// returns null, it's because another operation is currently in progress and
// you'll need to wait until it completes (or times out) before updating the
// shadow.
//
     if (clientTokenUpdate === null)
     {
        console.log('update shadow failed, operation still in progress');
     }
     }, 5000 );

     setInterval(function() {
 		thingShadows.publish('scentair/things/microgateway1', JSON.stringify(
             {
                 "ts":new Date().getTime(),
                 "type":"data",
                 "values":[
                     {
                         "name":"voltage",
                         "value":Math.random()
                     },
                     {
                         "name":"current",
                         "value":Math.random()
                     },
                     {
                         "name":"fan_speed",
                         "value":Math.random()
                     },
                     {
                         "name":"fan_current",
                         "value":Math.random()
                     },
                     {
                         "name":"pump_speed",
                         "value":Math.random()
                     },
                     {
                         "name":"pump_current",
                         "value":Math.random()
                     },
                     {
                         "name":"liquid_fluid",
                         "value":Math.random()
                     }
                 ]
             }), {"qos":1});	//end of publish
         console.log("Message Published...");
       },10000);	//end of setInterval
  });//end of the on connect

thingShadows.on('status',
  function(thingName, stat, clientToken, stateObject) {
     console.log('received '+stat+' on '+thingName+': '+
                 JSON.stringify(stateObject));
//
// These events report the status of update(), get(), and delete()
// calls.  The clientToken value associated with the event will have
// the same value which was returned in an earlier call to get(),
// update(), or delete().  Use status events to keep track of the
// status of shadow operations.
//
  });

thingShadows.on('delta',
  function(thingName, stateObject) {
     console.log('received delta on '+thingName+': '+
                 JSON.stringify(stateObject));
  console.log(stateObject.state.turnON);
  var rgbLedLampState = {"state":{"reported":{"turnON":stateObject.state.turnON}}};

     clientTokenUpdate = thingShadows.update('MicroGateway1', rgbLedLampState  );
  });

thingShadows.on('timeout',
  function(thingName, clientToken) {
     console.log('received timeout on '+thingName+
                 ' with token: '+ clientToken);
//
// In the event that a shadow operation times out, you'll receive
// one of these events.  The clientToken value associated with the
// event will have the same value which was returned in an earlier
// call to get(), update(), or delete().
//
  });
